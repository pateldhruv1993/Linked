$(document).ready(function () {

    var defaultContentDivWidth = $("#mainFeedContainer").width();
    var sideBarAnimation = true;
    $("#sideBarToggleButton").click(function () {

        if (sideBarAnimation == true) {
            sideBarAnimation = false;
            var toggleWidth = $(".sideBarListsNames").width() == 112 ? "0px" : "112px";
            $(".sideBarListsNames").animate({width: toggleWidth}, {duration: 150, easing: "swing", complete: function(){
                    sideBarAnimation = true;
            }});

            var newContentFeedWidth = $("#mainFeedContainer").width() == defaultContentDivWidth ? (defaultContentDivWidth + 112) + "px" : defaultContentDivWidth + "px";
            $("#mainFeedContainer").animate({width: newContentFeedWidth}, {duration: 150, easing: "swing"});
        }
    });
});