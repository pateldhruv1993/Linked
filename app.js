var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');

var youtube = require('youtube-api');
var Opn = require("opn");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);













//YOUTUBE-API stuff

var oauth = youtube.authenticate({
    type: "oauth"
  , client_id: '1094098679650-semhtr5141a5k48d5kkmq7087gminv4r.apps.googleusercontent.com'
  , client_secret: 'K9VmzBrffytuBUIzuTNao2nb'
  , redirect_url: 'http://goo.gl/UHUoBY'
});
















app.get('/auth/', function(){
    console.log("AUTH URL?!: " + oauth.generateAuthUrl({
        access_type: "offline",
        scope: ["https://www.googleapis.com/auth/youtube.upload"]
    }));
    Opn(oauth.generateAuthUrl({
        access_type: "offline",
        scope: ["https://www.googleapis.com/auth/youtube.upload"]
    }))
});

app.get('/callback',
  function(req, res) {
    
    oauth.getToken(req.params[0], function(err, tokens) {
        console.log("Req Param[0]: " - req.params[0]);
        if (err) { 
            return console.log(err); 
        }
        oauth.setCredentials(tokens);
        
    });
    res.send(req.params[0]);
    res.end();
  });











// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;